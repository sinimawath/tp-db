
CREATE EXTENSION IF NOT EXISTS citext;

DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS vote;
DROP TABLE IF EXISTS thread;
DROP TABLE IF EXISTS forum_user;
DROP TABLE IF EXISTS forum;
DROP TABLE IF EXISTS "user";

CREATE TABLE "user" (
  nickname citext PRIMARY KEY COLLATE "POSIX",
  fullname text,
  about text,
  email citext unique not null
);

CREATE TABLE forum (
  user_nick   citext references "user" not null,
  slug        citext PRIMARY KEY,
  title       text not null,
  thread_count integer default 0 not null,
  post_count integer default 0 not null
);

CREATE TABLE forum_user (
  nickname citext references "user" COLLATE "POSIX",
  forum_slug citext references "forum",
  CONSTRAINT unique_forum_user UNIQUE (forum_slug, nickname)
);

CREATE TABLE thread (
  id BIGSERIAL PRIMARY KEY,
  slug citext unique ,
  forum_slug citext references forum not null,
  user_nick citext references "user" not null,
  created timestamp with time zone default now(),
  title text not null,
  votes integer default 0 not null,
  message text not null
);

CREATE INDEX idx_thread_f_slug ON thread(forum_slug, created);

CREATE TABLE vote (
  nickname citext references "user",
  voice boolean not null,
  thread_id integer references thread,
  CONSTRAINT unique_vote UNIQUE (nickname, thread_id)
);

CREATE INDEX idx_vote ON vote(thread_id, voice);

CREATE TABLE post (
  id BIGSERIAL PRIMARY KEY,
  path integer[],
  author citext references "user",
  created timestamp with time zone,
  edited boolean,
  message text,
  parent_id integer default 0 references  post (id) NOT NULL,
  forum_slug citext,
  thread_id integer references thread
);

INSERT INTO post (id) VALUES (0);

CREATE INDEX idx_post_thread_id ON post(thread_id, id);
CREATE INDEX idx_posts_thread_parent_id ON post(thread_id, parent_id, id);
CREATE INDEX idx_posts_thread_path ON post(thread_id, path);
CREATE INDEX idx_post_t_p_path ON post(id, path);
CREATE INDEX idx_post_t_p_path_path_id ON post(thread_id, parent_id, (path[1]), id);

CREATE OR REPLACE FUNCTION change_edited_post() RETURNS trigger as $change_edited_post$
BEGIN
  IF NEW.message <> OLD.message THEN
    NEW.edited = true;
  END IF;
  
  return NEW;
END;
$change_edited_post$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS change_edited_post ON post;

CREATE TRIGGER change_edited_post BEFORE UPDATE ON post
  FOR EACH ROW EXECUTE PROCEDURE change_edited_post();

CREATE OR REPLACE FUNCTION create_path() RETURNS trigger as $create_path$
BEGIN
   IF NEW.parent_id = 0 THEN
     NEW.path := (ARRAY [NEW.id]);
     return NEW;
   end if;

   NEW.path := (SELECT array_append(p.path, NEW.id::integer)
                from post p where p.id = NEW.parent_id);
  RETURN NEW;
END;
$create_path$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS create_path ON post;

CREATE TRIGGER create_path BEFORE INSERT ON post
  FOR EACH ROW EXECUTE PROCEDURE create_path();