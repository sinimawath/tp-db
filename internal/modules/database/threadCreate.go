package database

import (
	"github.com/SinimaWath/tp-db/internal/models"
	"gopkg.in/jackc/pgx.v2"
)

const (
	insertThread = `
	INSERT INTO thread (slug, user_nick, created, forum_slug, title, message) 
	VALUES ($1,
	(SELECT u.nickname FROM "user" u WHERE u.nickname = $2),
	$3,
	(SELECT f.slug FROM forum f WHERE f.slug = $4)	
	,$5, $6)
	RETURNING id, slug, user_nick, created, forum_slug, title, message, votes
	`
)

func ThreadCreate(db *pgx.ConnPool, thread *models.Thread) error {

	tx, _ := db.Begin()
	tx.Exec("SET LOCAL synchronous_commit TO OFF;")
	err := scanThread(tx.QueryRow(insertThread, slugToNullable(thread.Slug), thread.Author,
		thread.Created, thread.Forum,
		thread.Title, thread.Message), thread)

	if err != nil {
		tx.Rollback()
		if err, ok := err.(pgx.PgError); ok {
			switch err.Code {
			case pgErrCodeNotNullViolation, pgErrForeignKeyViolation:
				return ErrNotFound
			case pgErrCodeUniqueViolation:
				err := SelectThreadBySlug(db, thread)
				if err != nil {
					return err
				}
				return ErrConflict
			}
		}
		return err
	}

	err = forumUpdateThreadCount(tx, thread.Forum)

	if err != nil {
		tx.Rollback()
		return err
	}

	err = createForumUserTx(tx, thread.Author, thread.Forum)

	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	increaseThreadCount()
	return nil
}
