package database

import (
	"github.com/SinimaWath/tp-db/internal/models"
	pgx "gopkg.in/jackc/pgx.v2"
)

// id, author, created, edited, message, parent_id, thread_id, forum_slug
func scanPostRows(r *pgx.Rows, post *models.Post) error {
	err := r.Scan(&post.ID, &post.Author, &post.Created, &post.IsEdited,
		&post.Message, &post.Parent, &post.Thread, &post.Forum)
	return err
}

func scanPost(r *pgx.Row, post *models.Post) error {
	err := r.Scan(&post.ID, &post.Author, &post.Created, &post.IsEdited,
		&post.Message, &post.Parent, &post.Thread, &post.Forum)
	return err
}
