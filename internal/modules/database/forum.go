package database

import (
	"github.com/SinimaWath/tp-db/internal/models"
	pgx "gopkg.in/jackc/pgx.v2"
)

func scanForum(r *pgx.Row, f *models.Forum) error {
	return r.Scan(
		&f.User,
		&f.Slug,
		&f.Title,
		&f.Threads,
		&f.Posts,
	)
}

const (
	checkForumExistQuery = `SELECT FROM forum WHERE slug = $1`
)

func checkForumExist(db *pgx.ConnPool, slug string) (bool, error) {
	err := db.QueryRow(checkForumExistQuery, slug).Scan()
	if err == pgx.ErrNoRows {
		return false, nil
	}
	return true, nil
}
