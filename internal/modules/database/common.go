package database

import (
	"errors"
	"sync/atomic"

	"gopkg.in/jackc/pgx.v2"

	"github.com/SinimaWath/tp-db/internal/models"
)

const (
	pgErrCodeUniqueViolation  = "23505"
	pgErrForeignKeyViolation  = "23503"
	pgErrCodeNotNullViolation = "23502"
)

var (
	ErrNotFound = errors.New("n")
	ErrConflict = errors.New("c")
)

var forumCount int32
var threadCount int32
var postCount int32
var userCount int32

const clearQuery = `TRUNCATE ONLY post, vote, thread, forum_user, forum, "user"`

func Clear(db *pgx.ConnPool) error {
	_, err := db.Exec(clearQuery)
	db.Exec("INSERT INTO post (id) VALUES (0)")
	atomic.SwapInt32(&forumCount, 0)
	atomic.SwapInt32(&threadCount, 0)
	atomic.SwapInt32(&postCount, 0)
	atomic.SwapInt32(&userCount, 0)

	return err
}

func Status(db *pgx.ConnPool, s *models.Status) error {

	s.Forum = atomic.LoadInt32(&forumCount)
	s.Thread = atomic.LoadInt32(&threadCount)
	s.Post = atomic.LoadInt32(&postCount)
	s.User = atomic.LoadInt32(&userCount)

	return nil
}

func increaseForumCount() {
	atomic.AddInt32(&forumCount, 1)
}

func increaseThreadCount() {
	atomic.AddInt32(&threadCount, 1)
}

func increaseUserCount() {
	atomic.AddInt32(&userCount, 1)
}

func increasePostCount(count int32) {
	atomic.AddInt32(&postCount, count)
}
