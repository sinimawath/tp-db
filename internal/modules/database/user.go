package database

import (
	"github.com/SinimaWath/tp-db/internal/models"
	pgx "gopkg.in/jackc/pgx.v2"
)

// Последовательность Nickname Fullname About Email
func scanUser(r *pgx.Row, user *models.User) error {
	return r.Scan(
		&user.Nickname,
		&user.Fullname,
		&user.About,
		&user.Email,
	)
}

// Последовательность Nickname Fullname About Email
func scanUserRows(r *pgx.Rows, user *models.User) error {
	return r.Scan(
		&user.Nickname,
		&user.Fullname,
		&user.About,
		&user.Email,
	)
}
