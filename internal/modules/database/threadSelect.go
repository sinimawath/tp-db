package database

import (
	"github.com/SinimaWath/tp-db/internal/models"
	pgx "gopkg.in/jackc/pgx.v2"
)

const (
	selectThreadByID = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread 
	WHERE id = $1
	`

	selectThreadBySlug = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread 
	WHERE slug = $1
	`

	selectThreadIDBySlug = `
	SELECT id
	FROM thread 
	WHERE slug = $1
	`
)

func SelectThreadBySlugOrID(db *pgx.ConnPool, slugOrID string, t *models.Thread) error {
	if id, isID := isID(slugOrID); !isID {
		t.Slug = slugOrID
		return SelectThreadBySlug(db, t)
	} else {
		t.ID = int32(id)
		return SelectThreadByID(db, t)
	}
}

func SelectThreadByID(db *pgx.ConnPool, t *models.Thread) error {
	err := scanThread(db.QueryRow(selectThreadByID, t.ID), t)

	if err == pgx.ErrNoRows {
		return ErrNotFound
	}

	return err
}

func SelectThreadBySlug(db *pgx.ConnPool, t *models.Thread) error {
	err := scanThread(db.QueryRow(selectThreadBySlug, t.Slug), t)

	if err == pgx.ErrNoRows {
		return ErrNotFound
	}

	return err
}

func SelectThreadIDBySlug(db *pgx.ConnPool, slug string) (int, error) {
	id := -1
	err := db.QueryRow(selectThreadIDBySlug, slug).Scan(&id)
	if err == pgx.ErrNoRows {
		return 0, ErrNotFound
	}
	return id, err
}

const (
	selectAllThreads = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1
	ORDER BY created
	`

	selectAllThreadsDesc = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1
	ORDER BY created DESC
	`

	selectAllThreadsLimit = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1
	ORDER BY created
	LIMIT $2
	`

	selectAllThreadsLimitDesc = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1
	ORDER BY created DESC
	LIMIT $2
	`

	selectAllThreadsSince = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1 AND created >= $2
	ORDER BY created
	`

	selectAllThreadsSinceDesc = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1 AND created <= $2
	ORDER BY created DESC
	`

	selectAllThreadsSinceLimit = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1 AND created >= $2
	ORDER BY created
	LIMIT $3
	`

	selectAllThreadsSinceLimitDesc = `
	SELECT id, slug, user_nick, created, forum_slug, title, message, votes
	FROM thread
	WHERE forum_slug = $1 AND created <= $2
	ORDER BY created DESC
	LIMIT $3
	`
)

func SelectAllThreadsByForum(db *pgx.ConnPool, slug string, limit int, desc bool,
	since string, ts *models.Threads) error {

	if isExist, _ := checkForumExist(db, slug); !isExist {
		return ErrNotFound
	}

	var rows *pgx.Rows
	if desc == true {
		if limit > 0 && since != "" {
			rows, _ = db.Query(selectAllThreadsSinceLimitDesc, slug, since, limit)
		} else if limit > 0 {
			rows, _ = db.Query(selectAllThreadsLimitDesc, slug, limit)
		} else if since != "" {
			rows, _ = db.Query(selectAllThreadsSinceDesc, slug, since)
		} else {
			rows, _ = db.Query(selectAllThreadsDesc, slug)
		}
	} else {
		if limit > 0 && since != "" {
			rows, _ = db.Query(selectAllThreadsSinceLimit, slug, since, limit)
		} else if limit > 0 {
			rows, _ = db.Query(selectAllThreadsLimit, slug, limit)
		} else if since != "" {
			rows, _ = db.Query(selectAllThreadsSince, slug, since)
		} else {
			rows, _ = db.Query(selectAllThreads, slug)
		}
	}

	defer rows.Close()

	for rows.Next() {
		thread := &models.Thread{}
		scanThreadRows(rows, thread)
		*ts = append(*ts, thread)
	}

	return nil
}
