package database

import (
	"github.com/SinimaWath/tp-db/internal/models"
	pgx "gopkg.in/jackc/pgx.v2"
)

const (
	selectUser = `
	SELECT nickname, fullname, about, email
	FROM "user"
	WHERE nickname = $1
	`

	selectUsersWithNickOrEmail = `
	SELECT nickname, fullname, about, email
	FROM "user"
	WHERE nickname = $1 OR email = $2
	`
)

func SelectUser(db *pgx.ConnPool, user *models.User) error {
	err := scanUser(db.QueryRow(selectUser, user.Nickname), user)
	if err == pgx.ErrNoRows {
		return ErrNotFound
	}

	return err
}

func SelectUsersWithNickOrEmail(db *pgx.ConnPool, nick, email string) (models.Users, error) {
	rows, err := db.Query(selectUsersWithNickOrEmail, nick, email)
	if err != nil {
		return nil, err
	}
	users := models.Users{}

	defer rows.Close()
	for rows.Next() {
		user := &models.User{}
		err := scanUserRows(rows, user)

		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}

const (
	selectAllUsersByForum = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1
	ORDER BY fu.nickname`

	selectAllUsersByForumDesc = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1
	ORDER BY fu.nickname DESC`

	selectAllUsersByForumLimit = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1
	ORDER BY fu.nickname
	LIMIT $2`

	selectAllUsersByForumLimitDesc = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1
	ORDER BY fu.nickname DESC
	LIMIT $2`

	selectAllUsersByForumLimitSince = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1 AND u.nickname > $2
	ORDER BY fu.nickname
	LIMIT $3`

	selectAllUsersByForumLimitSinceDesc = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1 AND u.nickname < $2
	ORDER BY fu.nickname DESC
	LIMIT $3`

	selectAllUsersByForumSince = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1 AND u.nickname > $2
	ORDER BY fu.nickname`

	selectAllUsersByForumSinceDesc = `
	SELECT u.nickname, u.fullname, u.about, u.email
	FROM forum_user fu
	JOIN "user" u ON fu.nickname = u.nickname
	WHERE fu.forum_slug = $1 AND u.nickname < $2
	ORDER BY fu.nickname DESC`
)

func SelectAllUsersByForum(db *pgx.ConnPool, slug string, limit int, desc bool, since string,
	users *models.Users) error {

	if isExist, _ := checkForumExist(db, slug); !isExist {
		return ErrNotFound
	}

	var rows *pgx.Rows
	if desc == true {
		if since != "" && limit > 0 {
			rows, _ = db.Query(selectAllUsersByForumLimitSinceDesc, slug, since, limit)
		} else if since != "" {
			rows, _ = db.Query(selectAllUsersByForumSinceDesc, slug, since)
		} else if limit > 0 {
			rows, _ = db.Query(selectAllUsersByForumLimitDesc, slug, limit)
		} else {
			rows, _ = db.Query(selectAllUsersByForumDesc, slug)
		}
	} else {
		if since != "" && limit > 0 {
			rows, _ = db.Query(selectAllUsersByForumLimitSince, slug, since, limit)
		} else if since != "" {
			rows, _ = db.Query(selectAllUsersByForumSince, slug, since)
		} else if limit > 0 {
			rows, _ = db.Query(selectAllUsersByForumLimit, slug, limit)
		} else {
			rows, _ = db.Query(selectAllUsersByForum, slug)
		}
	}

	defer rows.Close()

	for rows.Next() {
		user := &models.User{}
		scanUserRows(rows, user)
		*users = append(*users, user)
	}
	return nil
}
