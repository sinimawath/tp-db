package service

import (
	"github.com/SinimaWath/tp-db/internal/models"
	"github.com/SinimaWath/tp-db/internal/modules/database"
	"github.com/valyala/fasthttp"
)

func (self *ForumPgsql) Clear(ctx *fasthttp.RequestCtx) {
	database.Clear(self.db)
	ctx.SetContentType("application/json")
	ctx.SetStatusCode(fasthttp.StatusOK)
	return
}

func (self *ForumPgsql) Status(ctx *fasthttp.RequestCtx) {
	status := &models.Status{}
	database.Status(self.db, status)
	resp(ctx, status, fasthttp.StatusOK)
	return
}
