package service

import (
	"encoding/json"

	"github.com/valyala/fasthttp"
)

func marshalResp(resp json.Marshaler) []byte {
	json, _ := resp.MarshalJSON()
	return json
}

func resp(c *fasthttp.RequestCtx, resp json.Marshaler, status int) {
	c.SetContentType("application/json")
	c.SetStatusCode(status)
	c.Write(marshalResp(resp))
}
