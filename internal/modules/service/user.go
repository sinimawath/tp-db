package service

import (
	"github.com/SinimaWath/tp-db/internal/models"
	"github.com/SinimaWath/tp-db/internal/modules/database"
	"github.com/valyala/fasthttp"
)

func (self *ForumPgsql) UserCreate(ctx *fasthttp.RequestCtx) {
	u := &models.User{}
	u.Nickname = ctx.UserValue("nickname").(string)
	u.UnmarshalJSON(ctx.PostBody())
	err := database.CreateUser(self.db, u)

	switch err {
	case database.ErrConflict:
		users, _ := database.SelectUsersWithNickOrEmail(self.db, u.Nickname, u.Email)
		resp(ctx, users, fasthttp.StatusConflict)
		return
	}

	resp(ctx, u, fasthttp.StatusCreated)
	return
}

func (self *ForumPgsql) UserGetOne(ctx *fasthttp.RequestCtx) {
	user := &models.User{}
	user.Nickname = ctx.UserValue("nickname").(string)
	err := database.SelectUser(self.db, user)

	if err == database.ErrNotFound {
		resp(ctx, Error, fasthttp.StatusNotFound)
		return
	}

	resp(ctx, user, fasthttp.StatusOK)
}

func (self *ForumPgsql) UserUpdate(ctx *fasthttp.RequestCtx) {
	user := &models.User{}
	user.Nickname = ctx.UserValue("nickname").(string)
	userUpdate := &models.UserUpdate{}
	userUpdate.UnmarshalJSON(ctx.PostBody())

	err := database.UpdateUser(self.db, user, userUpdate)
	switch err {
	case database.ErrNotFound:
		resp(ctx, Error, fasthttp.StatusNotFound)
		return
	case database.ErrConflict:
		resp(ctx, Error, fasthttp.StatusConflict)
		return
	}

	resp(ctx, user, fasthttp.StatusOK)
	return
}
